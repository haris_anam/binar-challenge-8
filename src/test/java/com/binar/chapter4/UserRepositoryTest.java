package com.binar.chapter4;

import com.binar.chapter4.implementation.UserServiceImpl;
import com.binar.chapter4.model.User;
import com.binar.chapter4.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class UserRepositoryTest {
    private UserServiceImpl userService;

    @Mock private UserRepository userRepository;
    static final String PRIMARY_USERNAME = "haris";
    static final String PRIMARY_EMAIL = "haris@gmail.com";
    static final String PRIMARY_PASSWORD = "admin";
    static final String SECONDARY_USERNAME = "haris2";
    static final String SECONDARY_EMAIL = "haris2@gmail.com";
    static final String SECONDARY_PASSWORD = "admin2";

    @Before
    public void init() {
        System.out.println("Initiating...");
    }

    @BeforeEach
    public void setUp(){
        userService = new UserServiceImpl(this.userRepository);
    }

    @Test
    public void addUser(){
        User user = new User(PRIMARY_USERNAME, PRIMARY_EMAIL, PRIMARY_PASSWORD);
        User savedUser = userService.addUser(user);
        User user2 = new User(SECONDARY_USERNAME, SECONDARY_EMAIL, SECONDARY_PASSWORD);
        User savedUser2 = userService.addUser(user2);
        Mockito.when(savedUser.getEmailAddress()).thenReturn(PRIMARY_EMAIL);
    }

    @Test
    public void getUsers(){
        userService.getAllUsers();
        Mockito.when(userService.getAllUsers().size()).thenReturn(2);
    }

    @Test
    public void updateUser(){
        User user = userService.getUserByUsername(SECONDARY_USERNAME);
        user.setUsername(SECONDARY_USERNAME + "_updated");
        userService.updateUser(SECONDARY_USERNAME, user);
        Mockito.when(userService.getUserByUsername(SECONDARY_USERNAME + "_updated").getId()).thenReturn(user.getId());
    }
}
