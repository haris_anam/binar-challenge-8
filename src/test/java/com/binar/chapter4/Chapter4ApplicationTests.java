package com.binar.chapter4;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    UserRepositoryTest.class,
    FilmRepositoryTest.class,
})
public class Chapter4ApplicationTests {
    @Test
    public void contextLoads() {
    }
}

