package com.binar.chapter4.controller;

import com.binar.chapter4.implementation.UserServiceImpl;
import com.binar.chapter4.model.User;
import com.binar.chapter4.repository.UserRepository;
import com.binar.chapter4.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {
    @Autowired
    UserServiceImpl service;

    @GetMapping("/")
    public Collection<User> findAll() {
        return service.getAllUsers();
    }

    @PostMapping(value = "/", consumes = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    public User storeUser(@RequestBody final User user){
        return service.addUser(user);
    }

    @PutMapping(value = "/{username}", consumes = {"application/json"})
    public User updateUser(@PathVariable("username") final String username, @RequestBody final User user){
        service.updateUser(username, user);
        return user;
    }

    @DeleteMapping(value = "/{username}", consumes = {"*/*"})
    public User deleteUser(@PathVariable("username") final String username){
        User user = service.getUserByUsername(username);
        service.deleteUserByUsername(username);
        return user;
    }

}
