package com.binar.chapter4.service;

import org.springframework.web.servlet.view.document.AbstractPdfView;

public interface InvoiceService {
    AbstractPdfView generateInvoice();
}
