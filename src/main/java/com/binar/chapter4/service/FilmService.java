package com.binar.chapter4.service;

import com.binar.chapter4.model.Film;
import com.binar.chapter4.model.Schedule;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FilmService {
    Film addFilm(Film film);
    Film getFilmByCode(String code);
    List<Film> getAllFilms();
    void updateFilm(String code, Film updatedFilm);
    List<Film> getShowingFilms();
    Film addSchedule(String filmCode, Schedule schedule);
    List<Schedule> getSchedules(String filmCode);
    void deleteFilmByCode(String code);
}
