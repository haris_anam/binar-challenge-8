package com.binar.chapter4.repository;

import com.binar.chapter4.model.ERole;
import com.binar.chapter4.model.Role;
import com.binar.chapter4.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository <Role, String> {
    Optional<Role> findByName(ERole name);
}
