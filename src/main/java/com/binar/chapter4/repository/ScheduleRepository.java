package com.binar.chapter4.repository;

import com.binar.chapter4.model.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Integer> {
    List<Schedule> findByFilm_FilmCodeEquals(String filmCode);

    long deleteByFilm_FilmCodeEquals(String filmCode);
}