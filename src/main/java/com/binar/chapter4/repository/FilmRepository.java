package com.binar.chapter4.repository;

import com.binar.chapter4.model.Film;
import com.binar.chapter4.model.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

@Repository
public interface FilmRepository extends JpaRepository <Film, String> {
    List<Film> findBySchedules_JamMulaiIsLessThanAndSchedules_JamSelesaiIsGreaterThanAndSchedulesNotEmpty(Timestamp jamMulai, Timestamp jamSelesai);
}
