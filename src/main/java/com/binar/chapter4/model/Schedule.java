package com.binar.chapter4.model;

import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
@Accessors(chain=true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "schedules")
public class Schedule implements Serializable {
    private static final long serialVersionUID = 3L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "schedule_id")
    private int scheduleId;
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="film_code")
    @Getter(AccessLevel.NONE)
    private Film film;
    @Column(nullable = false, name = "tanggal_tayang")
    private Date tanggalTayang;
    @Column(nullable = false, name = "jam_mulai")
    private Timestamp jamMulai;
    @Column(nullable = false, name = "jam_selesai")
    private Timestamp jamSelesai;
    @Column(nullable = false, name = "harga_tiket")
    private int hargaTiket;
    @OneToMany(mappedBy = "schedule")
    private List<Seat> seats;
}