package com.binar.chapter4.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Accessors(chain=true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "films")
public class Film implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(nullable = false, name = "film_code", unique = true)
    private String filmCode;
    @Column(nullable = false, name = "film_name")
    private String filmName;
    @OneToMany(mappedBy = "film")
    private List<Schedule> schedules;
}