package com.binar.chapter4.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
class SeatNumber implements Serializable {
    private static final long serialVersionUID = -3253362791427315016L;
    @Enumerated(EnumType.ORDINAL)
    private SeatRow row;
    private int col;
}
